
# Enter your confiuguration values for the script


# URL to check
monitorURL='https://jamesrlopes.com'

# Where to send the results
destURL='https://viz.jamesrlopes.com/dbUpdate.php'

# location of log file
logPath='./urlmon.log'

# Threshold percentage of disk space available before alerting
diskThresh=90 #percent

# How often do you want to check the availbility of the site
monitorFrequency=10 #seconds
