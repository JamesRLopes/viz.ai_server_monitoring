#!/bin/bash

# this is the config file we will read in from
source config.txt

# Print this so users know the program is running
echo -e "======================================================== 
		\n========== Starting URLMon, CTRL+C to stop ============= 
		\n========================================================\n"

# Loop time!
while true;
	do
		# Lets get the current disk space available
		# Since we have a configurable logPath we need to find out what partition the logging is on and check that
		# Also, create the log file if it doesn't exist
		touch $logPath
		diskUse=`df -h $logPath | awk '{print $5}'| tail -n1| cut -d '%' -f1`
		
		# Check disk space against our threshold
		if [ $diskUse -ge $diskThresh ]
			then
				if [ $diskUse = 100 ]
					then
						echo "!!! DISK FULL !!!" |tee -a $logPath
					else
						echo "!!! Available Disk space below threshold !!!" | tee -a $logPath
					fi
			fi	
		echo -n `date +%FT%T` ": " |tee -a $logPath
		echo -en "Status code of $monitorURL: " |tee -a $logPath
		
		# check the URL's http status using -I, use k to ignore insecure certs, and -L to follow redirects
		# need to set the timeout equal to the polling frequency to avoid backing up the task
		unset statusCode;
		statusCode=`curl -k --max-time $monitorFrequency -sIkL $monitorURL| grep HTTP| awk '{print $2}'| tail -n1`
		# if no response from the server lets report 000 so the other end knows we're down
		if [ -z "$statusCode" ]
			then statusCode=000; 
				curl -ks -d "siteName=$monitorURL&statCode=$statusCode&dskUse=$diskUse&dskThresh=$diskThresh" -X POST $destURL
				echo -e "Status code = '$statusCode'.\n!!!URL Unreachable!!!\n" |tee -a $logPath
			else
		echo -e "$statusCode\n" |tee -a $logPath
		
		#Lets make the request to the remote server to record the status
		curl -ksd "siteName=$monitorURL&statCode=$statusCode&dskUse=$diskUse&dskThresh=$diskThresh" -X POST $destURL	
		# lets check back in x seconds
		sleep $monitorFrequency;
		fi
	done
