
#######################################################
#####    This README is a guide on how to install #####
#####   the script responsible for monitoring the #####
#####         URL health of a system              #####
#######################################################

#### installing ####

# Download the package
To install the Viz.ai URL health monitor you will need to download the file from git using: git clone https://JamesRLopes@bitbucket.org/JamesRLopes/viz.ai_server_monitoring.git


# Configuring
There is a config file called config.txt which contains the relevant variables that will be passed to the script. The config is pre-populated with example values that should be replaced by the users. By default we will send our monitoring data to a system already configured to acccept requests.

Values to modify:

__monitorurl__: This is the URL to be monitored by the script. A full URI should be used
__destURL__: This is where we'll be sending the data
__logPath__: This value determines where to log the output to, by default we'll put it in the local directory
__diskThresh__: Set this to the value you wish to compare the current disk space against. When current disk space usage exceeds this value we will start alerting
__monitorFrequency__: How often do you want to check the URL? Note this is in seconds

# Running the script
The URLMon is a simple bash script that will, by default, run in the foreground. You should daemonize it by appending & to the end of the start command
```
/your/dir/URLMon.sh &
```

# Stopping the script
If you've run the script as a daemon (using the &) then you will need to find the process ID (PID) of the process, this can be done using "ps aux| grep URLMon.sh | grep -v grep". If you've run the script in your current session then you can simply CTRL+C to break

# Testing

You can test the functionality of this script by setting the destURL parameter equal to https://viz.jamesrlopes.com/dbUpdate.php and then find the results by navigating your favorite web browser to https://viz.jamesrlopes.com/index.php